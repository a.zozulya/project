// Функция внутри $( document ).ready() срабатывает после загрузки DOM
$(document).ready(function() {
  console.log( "ready!" );
  // Поиск элемента в DOM по его id и замена его содержимого innerHTML с помощью JQuery
  $("#message").html("Hello world!");
  
  // Обработка события клика по кнопкам
  $(".btn-slide").click(function () {
    // Переключение класса кнопки, по которой кликнули   
    $(this).toggleClass("active");
  });

  // Пример анимации  
  $(".pane .delete").click(function(){
     $(this).parents(".pane").animate({ opacity: "hide" }, "slow");
  });

  // "Аккордеон"
 $(".faq-section h2:first").addClass("active");
 $(".faq-section p:not(:first)").hide();
 $(".faq-section h2").click(function () {
   $(this).next("p").slideToggle("slow")
     .siblings("p:visible").slideUp("slow");
   $(this).toggleClass("active");
   $(this).siblings("h2").removeClass("active");
 });

    
}); 
