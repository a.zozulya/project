document.addEventListener('DOMContentLoaded', function() {

    
    let message = { // массив сообщений
        loading: 'Загрузка...',
        success: 'Данные успешно отправлены!',
        failure: 'Что-то пошло не так...'
    };
    let form = document.querySelector('.main'),  // доступ к форме
        statusMessage = document.createElement('div'), // создаем новый див для сообщения об отправке
        input = document.querySelector('input[type=submit]'),  // доступ к кнопке отправить
        inputs = document.querySelectorAll('input[type=text], input[type=email]'); // доступ к имени и почте
        statusMessage.style.marginTop='20px';
    form.appendChild(statusMessage);  // добавили в форму созданный див

    input.addEventListener('click', function(event) { // обработка кнопки отправки
        let key = inputs[0].value;// имя
        let value = inputs[1].value; // почта
        localStorage.setItem(key, value); // создаем новую запись

    });

    formcarry({ // отправка через fetch 
        form: "sioSiPsa-g", // моя ссылка
        element: "#my-formcarry",
        extraData: {
            screenSize: `${window.screen.width}x${window.screen.height}`,
            language: window.navigator.language,
        },
        onSuccess: function(response) {
            statusMessage.innerHTML = message.success;
             for (var i = 0; i < inputs.length; i++) {
                inputs[i].value = ''; // очищаем инпуты
            };

        },
        onError: function(error) {
            statusMessage.innerHTML = message.failure;
        }
    });


});

$(document).ready(function() {

    $('.recipes').slick({
        nextArrow: '<button type="button" class="slick-next"><img src="img/next.png" alt=""></button>',
        prevArrow: '<button type="button" class="slick-prev"><img src="img/prev.png" alt=""></button>',
        slidesToShow: 4,
        slidesToScroll: 4,
        dots: true,
        responsive: [{
            breakpoint: 769,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
            }
        }],
    });
});

